## Sec Section - Error Budget, Reliability, and Security Status Reporting

Each week our teams review current error budgets, active or overdue reliability issues, and active or overdue security issues. To provide a consistent reporting mechanism, we utilize workflows in Slack to ping the the team's slack channel. 

![image-3.png](./image-3.png)

## Steps to create a workflow

1. Navigate to https://app.slack.com/workflow-builder/
1. Import the example file found in this project
1. Customize slack channels and links to Grafana for your group. 

